class Stepper {
  constructor(selector) {
    this.stepper = $(selector);
    this.prevButton = this.stepper.find(".prev");
    this.nextButton = this.stepper.find(".next");

    this.init();
  }

  init() {
    this.addListener();
  }

  addListener() {
    const self = this;
    this.prevButton.on("click", function() {
      // console.log(self.back);
      self.goPrev($(this));
    });

    this.nextButton.on("click", function() {
      // console.log(self.next);
      self.goNext($(this));
    });
  }

  goPrev($this) {
    this.stepper.find(".step").removeClass("active");
    $this
      .closest(".step")
      .prev(".step")
      .addClass("active");
  }

  goNext($this) {
    this.stepper.find(".step").removeClass("active");
    $this
      .closest(".step")
      .next(".step")
      .addClass("active");
  }
}

export default Stepper;
